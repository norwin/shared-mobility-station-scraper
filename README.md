# carsharing-station-scraper

Get car- & bike-sharing station data

![map of all_stations.geojson](map.png)

## supported providers
| source  | operators | vehicle type | stations | freefloating | license |
|---------|-----------|--------------|----------|--------------|---------|
| api     | [flinkster](https://flinkster.de) + all partners, (eg teilAuto, JEZ!, ...) | car | y | no, but available | y |
| website | [StadtMobil](https://stadtmobil.de) + partners | car | y | n | ? |
| website | [Cambio](https://www.cambio-carsharing.de) | car | y | n | ? |
| map     | [Regio.Mobil](https://regio-mobil-deutschland.de) | car | y | n | ? |
| api     | [call-a-bike](https://callabike.de) + selected sub-brands, (eg. regiorad stuttgart) | bike | y | no, but available | ? |
| api     | [Stadtrad Hamburg](https://stadtrad.hamburg.de) | bike | y | n | y |
| api     | [Nextbike](https://nextbike.net) + sub-brands | bike | y | no, but available | y |
| api     | [Velocity Aachen](https://velocity-aachen.de) | bike | y | n | ? |
| website | [ESWE meinRad](https://www.eswe-verkehr.de/mobilitaet/fahrrad/app-und-co.html) | bike | y | n | ? |

## data
Data is serialized in two formats:
- `all_stations.geojson`: GeoJSON FeatureCollection of stations of all providers, with some normalized metadata attached
- `<provider>.yaml`: stations of each provider, grouped by city. formatted for CleverRoute configuration, without metadata.

Find a snapshot of the data [here](./data) (last data dump: 2021-06-28)

## usage
You'll need [node.js](https://nodejs.org) installed.
To fetch from flinkster, you'll [need an API key](https://developer.deutschebahn.com/store/apis/info?name=Flinkster_API_NG&version=v1&provider=DBOpenData).
Set this key via the environment variable `FLINKSTER_TOKEN`

```
FLINKSTER_TOKEN='foobar' npx git+https://git.nroo.de/ecolibro/carsharing-station-scraper
```

## showcase
```
> $ npm start

> carsharing-scrape@1.0.0 start /home/norwin/src/carsharing-scrape
> node index.js

> CAMBIO
>   fetching https://www.cambio-carsharing.de/cms/carsharing/de/1/cms_f2_2/stdws_info/stationen/
>   parsing html (1402632 bytes)
>     extracted 1334 stations
>   writing results to data/cambio.yaml
>   done after 2.055s
> FLINKSTER
>   fetching https://api.deutschebahn.com/flinkster-api-ng/v1/areas?expand=provider&type=station&providernetwork=1&limit=100&offset=0
>   fetching https://api.deutschebahn.com/flinkster-api-ng/v1/areas?expand=provider&type=station&providernetwork=1&limit=100&offset=100
>   fetching https://api.deutschebahn.com/flinkster-api-ng/v1/areas?expand=provider&type=station&providernetwork=1&limit=100&offset=200
>   found 1812 stations
>   writing results to data/flinkster.yaml
>   done after 11.165s
> REGIOMOBIL
>   fetching https://www.google.com/maps/d/kml?mid=1EDmZTRYO2VdZX3BFEksRwzibNwazCLg5&forcekml=1
>   found 71 stations
>     37 stations remain
>   writing results to data/regiomobil.yaml
>   done after 0.43s
> STADTMOBIL
>   fetching https://www.stadtmobil.de/marker
>   parsing html (793831 bytes)
>     parsed 1620 stations in 257 cities
>   converting to geojson
>   writing results to data/stadtmobil.yaml
>   done after 1.504s
> writing all_stations.geojson
```

## license
- code: GPL-2
- data: none, if in doubt ask the carsharing provider.
