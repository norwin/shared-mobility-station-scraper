const fetch = require('node-fetch')
const { point } = require('@turf/helpers')
const { log } = require('./util')
const operators = require('./operators')

module.exports = {
  fetchFlinksterAPI,
  flinksterAPIstationToGeojson,
}

// networkID 1 -> flinkster + partners
// networkID 2 -> call-a-bike + partners
async function fetchFlinksterAPI(networkID = 1) {
  const auth = process.env.FLINKSTER_TOKEN
  if (!auth) throw new Error('env FLINKSTER_TOKEN required, see https://developer.deutschebahn.com/store/apis/info?name=Flinkster_API_NG&version=v1&provider=DBOpenData#/')

  const limit = 100
  const baseURL = `https://api.deutschebahn.com/flinkster-api-ng/v1/areas?expand=provider&type=station&providernetwork=${networkID}&limit=${limit}`
  let stations = []
  let total = 1
  let offset = 0
  while (stations.length < total) {
    const url = `${baseURL}&offset=${offset}`
    log(`  fetching ${url}`)
    const req = await fetch(url, { headers: { Authorization: `Bearer ${auth}` } })
    const res = await req.json()
    if (req.status !== 200)
      throw new Error(res.errors[0].message)
    total = res.size
    offset += res.limit
    stations = stations.concat(res.items)
  }
  return stations
}

function flinksterAPIstationToGeojson (station) {
  const { name, href, geometry, address, provider } = station
  const props = {
    ...mapProviderToOperator(provider),
    source: 'de.flinkster',
    id: href,
    name,
    city: address.city,
  }
  return point(geometry.position.coordinates, props)
}

function mapProviderToOperator({ uid, href, name }) {
  if (uid.startsWith('fcs_') || uid.startsWith('fsc_'))
    return operators.get('ford')
  if (uid.startsWith('drive_carsharing'))
    return operators.get('drive')
  if (uid.startsWith('flinkster'))
    return operators.get('flinkster')
  if (uid.startsWith('einfach_mobil'))
    return operators.get('scouter')
  return operators.get(uid.toLowerCase()) || {
    operator: uid,
  }
}
