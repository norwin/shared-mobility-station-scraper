const { join } = require('path')
const { readFileSync, writeFile } = require('fs')
const { dump } = require('js-yaml')
const pointsInPoly = require('@turf/points-within-polygon')

function log (...args) {
  console.log(`> ${args.join(' ')}`)
}

function serializeCRYaml (file, features) {
  log(`  writing results to ${file}`)
  const yaml = dump(featuresToCleverRoute(features), { flowLevel: 3 })
  return new Promise((res, rej) => {
    writeFile(file, yaml, 'utf-8', err => err ? rej(err) : res())
  })
}

function featuresToCleverRoute (features) {
  const res = {}
  for (const feat of features) {
    const { city } = feat.properties
    if (!res[city]) res[city] = []
    res[city].push(featureToCROverrideLocation(feat))
  }
  return res
}

function featureToCROverrideLocation (feat) {
  return {
    name: feat.properties.name,
    lonLat: feat.geometry.coordinates,
  }
}

/**
 * 
 * @param {FeatureCollection<Point>} points
 * @param {string} polyFile pointing to a file with a GeoJSON FeatureCollection<Polygon>
 * @param {*} groupNameProp property to extract the polygon group name
 */
function groupByPoly (points, polyFile = join(__dirname, 'data/kreise.json'), groupNameProp = 'GEN') {
  const stations = []
  const groups = new Set()
  const polygons = JSON.parse(readFileSync(polyFile, 'utf-8'))
  for (const p of polygons.features) {
    const name = p.properties[groupNameProp]
    const matches = pointsInPoly(points, p)
    if (!matches.features.length)
      continue
    groups.add(name)
    for (const point of matches.features)
      point.properties.group = name
    stations.push(...matches.features) // FIXME: we may add duplicates for overlapping polys
  }
  return { stations, groups }
}

module.exports = {
  log,
  groupByPoly,
  serializeCRYaml,
}
