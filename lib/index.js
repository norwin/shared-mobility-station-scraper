const { basename, join } = require('path')
const { readdirSync } = require('fs')

// require all scrapers from ./scrapers/*, return them in an object by name
function loadScrapers () {
  const scrapersDir = join(__dirname, '../lib/scrapers')
  return readdirSync(scrapersDir)
    .filter(f => f.endsWith('.js'))
    .reduce((ss, f) => {
      ss[basename(f, '.js')] = require(join(scrapersDir, f))
      return ss
    }, {})
}

module.exports = {
  scrapers: loadScrapers(),
}
