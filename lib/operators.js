const operators = new Map()

operators.set('bodenseemobil', {
  operator: 'https://bodenseemobil.de',
  'marker-color': '#a37c8d',
})
operators.set('book-n-drive', { // alias city-flitzer?
  operator: 'https://book-n-drive.de',
  'marker-color': '#bb1b10',
})
operators.set('bundeswehr', { // uff ?!
  operator: 'https://bwcarsharing.de',
  'marker-color': '#8ca06e',
})
operators.set('cambio', {
  operator: 'https://cambio-carsharing.de',
  'marker-color': '#ff6801',
})
operators.set('drive', {
  operator: 'https://drive-carsharing.com',
  'marker-color': '#1f964a',
})
operators.set('e-wald', {
  operator: 'https://e-wald.eu',
  'marker-color': '#e5c437',
})
operators.set('flinkster', {
  operator: 'https://flinkster.de',
  'marker-color': '#f01414',
})
operators.set('ford', {
  operator: 'https://ford-carsharing.de',
  'marker-color': '#06507F',
})
operators.set('regiomobil', {
  operator: 'https://regio-mobil-deutschland.de',
  'marker-color': '#7ba433',
})
operators.set('scouter', { // früher einfach-mobil
  operator: 'https://scouter.de',
  'marker-color': '#FF5C00',
})
operators.set('sheeper_sharing', {
  operator: 'https://sheepersharing.com',
  'marker-color': '#88AA00',
})
operators.set('stadtmobil', {
  operator: 'https://stadtmobil.de',
  'marker-color': '#FF994A',
})
operators.set('teilauto', { // alias city-flitzer?
  operator: 'https://teilauto.net',
  'marker-color': '#75b42b',
})


operators.set('call_a_bike', {
  operator: 'https://callabike.de',
  vehicle: 'bike',
  'marker-color': '#f01414',
  'marker-symbol': 'bicycle',
})
operators.set('stadtrad_hamburg', {
  operator: 'https://deutschebahnconnect.com',
  city: 'Hamburg',
  vehicle: 'bike',
  'marker-color': '#f01414',
  'marker-symbol': 'bicycle',
})
operators.set('regiorad_stuttgart', {
  operator: 'https://regioradstuttgart.de',
  vehicle: 'bike',
  'marker-color': '#009fe4',
  'marker-symbol': 'bicycle',
})
operators.set('nextbike', {
  operator: 'https://nextbike.net',
  vehicle: 'bike',
  'marker-color': '#004a99',
  'marker-symbol': 'bicycle',
})
operators.set('eswe', {
  operator: 'https://eswe-verkehr.de',
  vehicle: 'bike',
  'marker-color': '#eb6707',
  'marker-symbol': 'bicycle',
})
operators.set('velocity', {
  operator: 'https://velocity-aachen.de',
  vehicle: 'bike',
  'marker-color': '#81b834',
  'marker-symbol': 'bicycle',
  city: 'Aachen'
})


module.exports = operators
