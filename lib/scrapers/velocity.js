const fetch = require('node-fetch')
const { point } = require('@turf/helpers')
const { log } = require('../util')
const operators = require('../operators')

module.exports = async function eswe () {
  let stations = await fetchAPI()
  log(`  found ${stations.length} stations`)
  stations = stations.filter(s => s.state === 'OPERATIVE')
  log(`    ${stations.length} stations operational`)
  return stations.map(stationToGeojson)
}

async function fetchAPI () {
  const url = `https://cms.velocity-aachen.de/backend/app/stations`
  const req = await fetch(url)
  return await req.json()
}

function stationToGeojson (s) {
  const { locationLatitude, locationLongitude, stationId, name } = s
  const props = {
    ...operators.get('velocity'),
    source: 'de.velocity-aachen',
    id: stationId,
    name,
  }
  return point([locationLongitude, locationLatitude], props)
}
