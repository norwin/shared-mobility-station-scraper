const fetch = require('node-fetch')
const { log } = require('../util')
const operators = require('../operators')

// although provided by call-a-bike / DB connect GmbH, their data is not contained in the flinkster API...
// as we didn't yet reverse-engineer the DB connect API (#10), we go with the open-data-portal here.

module.exports = async function () {
  const stations = await fetchOpenData()
  log(`  found ${stations.length} stations`)
  return stations.map(stadtradToGeojson)
}

async function fetchOpenData() {
  // source: https://suche.transparenz.hamburg.de/dataset/stadtrad-stationen-hamburg20?forceWeb=true
  // NOTE: this is a Sensor Things API (thanks 52N)
  // NOTE: we hardcode a result size here, but we shoul
  const count = 2000
  const url = `https://iot.hamburg.de/v1.0/Things?$filter=Datastreams/properties/serviceName%20eq%20%27HH_STA_StadtRad%27&$orderby=id&$count=true&$expand=Locations&$top=${count}`
  log(`  fetching ${url}`)
  const req = await fetch(url)
  const res = await req.json()
  if (res['@iot.count'] > count)
    log('    WARNING: did not fetch all available stations')
  return res.value
}

function stadtradToGeojson(station) {
  const { location, name, } = station.Locations[0] // there's always one.
  location.properties = {
    name,
    source: 'de.hamburg',
    id: station.properties.assetID,
    ...operators.get('stadtrad_hamburg'),
  }
  return location
}
