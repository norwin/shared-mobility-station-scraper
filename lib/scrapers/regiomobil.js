const { toJson: parseKml } = require('parse-kml')
const { log } = require('../util')

module.exports = function regiomobil () {
  // sourced from https://www.regio-mobil-deutschland.de/stationskarte
  const url = 'https://www.google.com/maps/d/kml?mid=1EDmZTRYO2VdZX3BFEksRwzibNwazCLg5&forcekml=1'
  log(`  fetching ${url}`)
  return parseKml(url).then(filterResults)
}

async function filterResults (geojson) {
  // stations are grouped by icon in kml, these icons are valid
  const validStyleUrls = [
    '#icon-1899-0288D1', // regio.mobil pkw
    '#icon-1899-FFEA00', // regio9er
  ]
  log(`  found ${geojson.features.length} stations`)
  const result = geojson.features
    .filter(f => validStyleUrls.includes(f.properties.styleUrl))
    .map(f => {
      // TODO: id, group
      f.properties = {
        name: f.properties.name,
        operator: 'de.regiomobil',
        'marker-color': '#7ba433',
      }
      return f
    })
  log(`    ${result.length} stations remain`)
  return result
}
