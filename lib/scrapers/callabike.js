const { log } = require('../util')
const { fetchFlinksterAPI, flinksterAPIstationToGeojson } = require('../flinkster-api')

module.exports = async function flinkster () {
  let stations = await fetchFlinksterAPI(2)
  log(`  found ${stations.length} stations`)
  return stations.map(flinksterAPIstationToGeojson)
}
