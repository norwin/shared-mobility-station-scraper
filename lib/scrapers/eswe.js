const fetch = require('node-fetch')
const { point } = require('@turf/helpers')
const { log } = require('../util')
const operators = require('../operators')

module.exports = async function eswe () {
  const stations = await fetchAPI()
  log(`  found ${stations.length} stations`)
  return stations.map(stationToGeojson)
}

async function fetchAPI () {
  // url extracted from https://www.eswe-verkehr.de/mobilitaet/fahrrad/app-und-co.html, where it is accessible as
  //   JSON.parse(document.querySelector('.nnaddress-leaflet-map').dataset['layers'])['meinrad']['marker']
  // NOTE: the response format is suspiciously similar to that of nextbike..

  // const url = `https://www.eswe-verkehr.de/mobilitaet/fahrrad/app-und-co.html?type=99020&action=getPins&format=json&details=min&category=&cHash=9e6b5fbd1e0db73ba26c8f1a85788d3b`
  const url = `https://www.eswe-verkehr.de/mobilitaet/fahrrad/app-und-co.html?type=99020&action=getPins&format=json&details=max`
  const req = await fetch(url)
  const res = await req.json()
  return res.entries
}

function stationToGeojson (s) {
  const { lat, lng, uid, name, title, city } = s
  const props = {
    ...operators.get('eswe'),
    source: 'de.eswe-verkehr',
    id: uid,
    name: name || title,
    city,
  }
  return point([lng, lat].map(Number.parseFloat), props)
}
