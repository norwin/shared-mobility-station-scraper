const fetch = require('node-fetch')
const { JSDOM } = require('jsdom')
const { point } = require('@turf/helpers')
const { log } = require('../util')
const operators = require('../operators')

module.exports = function stadtmobil () {
  const url = 'https://www.stadtmobil.de/marker'
  log(`  fetching ${url}`)
  return fetch(url)
    .then(res => res.text())
    .then(parseHtmlMarkers)
    // .then(filterThirdParty)
    .then(toGeojson)
}

async function parseHtmlMarkers (html) {
  log(`  parsing html (${html.length} bytes)`)
  const cities = {}
  const cityIds = {}
  const dom = new JSDOM(html)

  let numStations = 0

  dom.window.document.querySelectorAll('.city-marker').forEach(el => {
    const city = JSON.parse(JSON.stringify(el.dataset))

    let id = city.name
    if (id in cityIds) id = `${city.name} - ${city.uid}`
    cityIds[city.uid] = id

    city.longitude = parseFloat(city.longitude)
    city.latitude = parseFloat(city.latitude)
    city.stations = []
    cities[id] = city
  })

  dom.window.document.querySelectorAll('.station-marker').forEach(el => {
    const s = JSON.parse(JSON.stringify(el.dataset))
    const city = cities[cityIds[s.city]]
    city.stations.push(s)

    s.city = city.name
    if (s.carproducts)
      s.carproducts = JSON.parse(s.carproducts)
    if (!s.partner && city.partner)
      s.partner = city.partner
    numStations++
  })

  log(`    parsed ${numStations} stations in ${Object.keys(cities).length} cities`)
  return cities
}

async function filterThirdParty (cities) {
  log(`  removing third-party stations`)
  let numStations = 0
  for (const id in cities) {
    if (cities[id].partner) {
      delete cities[id]
      continue
    }

    for (let s of cities[id].stations) {
      if (s.partner)
        s = null
      else
        numStations++
    }
  }

  log(`    ${numStations} stations in ${Object.keys(cities).length} cities remain`)

  return cities
}

async function toGeojson (cities) {
  log(`  converting to geojson`)
  const features = []
  for (const city of Object.values(cities)) {
    for (const s of city.stations) {
      const props = {
        source: 'de.stadtmobil',
        name: s.name,
        city: s.city,
        id: s.uid,
        carproducts: s.carproducts, // TODO: resolve IDs maybe (s.organization too)
        ...operators.get(s.partner ? 'null' : 'stadtmobil'),
      }
      features.push(point([s.longitude, s.latitude].map(Number.parseFloat), props))
    }
  }
  return features
}
