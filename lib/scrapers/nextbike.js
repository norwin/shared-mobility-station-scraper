const fetch = require('node-fetch')
const { point } = require('@turf/helpers')
const { log } = require('../util')
const operators = require('../operators')

module.exports = async function nextbike () {
  let countries = await fetchAPI(1)
  countries = countries.filter(c => c.language === 'de') // only keep german speaking countries
  const stations = flattenStations(countries)
  log(`  found ${stations.length} stations`)
  return stations.map(stationToGeojson)
}

async function fetchAPI () {
  const url = `https://api.nextbike.net/maps/nextbike-official.json`
  const req = await fetch(url)
  const res = await req.json()
  return res.countries
}

function flattenStations (countries) {
  // note: countries are actually sub-brands..
  const stations = []
  for (const country of countries) {
    for (const city of country.cities) {
      for (const s of city.places) {
        s.city = city.name
        stations.push(s)
      }
    }
  }
  return stations
}

function stationToGeojson (s) {
  const { lat, lng, uid, name, city } = s
  const props = {
    ...operators.get('nextbike'),
    source: 'net.nextbike',
    id: uid,
    name,
    city,
  }
  return point([lng, lat], props)
}
