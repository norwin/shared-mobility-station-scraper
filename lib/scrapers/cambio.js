const fetch = require('node-fetch')
const { JSDOM } = require('jsdom')
const { point } = require('@turf/helpers')
const { log } = require('../util')
const operators = require('../operators')

module.exports = function cambio () {
  const url = 'https://www.cambio-carsharing.de/cms/carsharing/de/1/cms_f2_2/stdws_info/stationen/'
  log(`  fetching ${url}`)
  return fetch(url)
    .then(res => res.text())
    .then(parseHtml)
    .then(stations => stations.map(stationToGeojson))
}

async function parseHtml(html) {
  log(`  parsing html (${html.length} bytes)`)

  const dom = new JSDOM(html, {
    // mute JS eval errors (trying to access $, which is not available as we dont load remote scripts)
    beforeParse (window) {
      window.onerror = () => true
    },
    // stations are rendered serverside into the html as javascript. (oof)
    // to get all stations, call getStations() on the page context.
    // after checking the site, running scripts is safe, as mostly functions are
    // declared only, though we are technically opening for RCE here..
    runScripts: 'dangerously',
  })

  const stations = dom.window.getStations()
  log(`    extracted ${stations.length} stations`)
  return stations
}

function stationToGeojson (station) {
  const { id, lat, lng, name, ort } = station
  return point([lng, lat].map(Number.parseFloat), {
    source: 'de.cambio',
    name,
    id,
    city: ort,
    ...operators.get('cambio'),
  })
}
